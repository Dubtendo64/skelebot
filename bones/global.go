package bones

import (
	"github.com/bwmarrin/discordgo"
)

var (
	// Global config
	Config Configuration

	// Global Cringe Unified Manager
	CringeState CUM

	// Global variables
	PREFIX  = "."
	Session *discordgo.Session
	Guild   *discordgo.Guild
)

// Global constants
const (
	INFO              = "[INFO] "
	WARN              = "[WARN] "
	ERR               = "[ERROR] "
	COM               = "[COMMAND] "
	HTTP              = "[HTTP] "
	TODO              = "[TODOIST] "
	YOUTUBE           = "[YOUTUBE] "
	GOOGLE            = "[GOOGLE] "
	PHISH             = "[PHISH] "
	EIGHTBALL         = "[8BALL] "
	PICK              = "[PICK] "
	DICE              = "[DICE] "
	CHOOSE            = "[CHOOSE] "
	EXPLODE           = "[EXPLODE] "
	CRINGE            = "[CRINGE] "
	ANIME             = "[ANIME] "
	TOUHOU            = "[TOUHOU] "
	MORSE             = "[MORSE] "
	WEATHER           = "[WEATHER] "
	STATUS            = "[STATUS] "
	SEC               = "[SECURITY] "
	SEARCH            = "[SEARCH] "
	TEAM              = "[TEAM] "
	BRAIN             = "[BRAIN] "
	PHISHERMAN_CHECK  = "https://api.phisherman.gg/v2/domains/check/"
	PHISHERMAN_REPORT = "https://api.phisherman.gg/v2/phish/report"
	TODOIST_URL       = "https://api.todoist.com/rest/v1/tasks"
	ANILIST_QUERY     = "{\"query\":\"query ($page: Int $perPage: Int, $search: String) {\\r\\n    Page(page: $page, perPage: $perPage) {\\r\\n        pageInfo {\\r\\n            total\\r\\n            perPage\\r\\n        }\\r\\n        media(search: $search, type: ANIME, sort: FAVOURITES_DESC) {\\r\\n            id\\r\\n            title {\\r\\n                romaji\\r\\n                english\\r\\n                native\\r\\n            }\\r\\n            type\\r\\n            genres\\r\\n        }\\r\\n    }\\r\\n}\""
	APIVOID_URL       = "https://endpoint.apivoid.com/urlrep/v1/pay-as-you-go/"
	SKELE             = `
               Skele's Choice Skelebot
                       ______
                    .-"      "-.
                   /            \
                  |              |
                  |,  .-.  .-.  ,|
                  | )(__/  \__)( |
                  |/     /\     \|
                  (_     ^^     _)
                   \__|IIIIII|__/
                    | \IIIIII/ |
                    \          /
                     '--------'
        Skele's Choice, it's the best choice!
                  Ask your bones!`
)

// Config type
type Configuration struct {
	Prefix             string   `yaml:"prefix"`          // Bot prefix
	Config_File        string   `yaml:"config"`          // Config location
	Token              string   `yaml:"token"`           // Discord API key
	Logging            string   `yaml:"logging"`         // Logging channel ID
	Learning           bool     `yaml:"learning"`        // If learning is enabled or not.
	Todoist            string   `yaml:"todoist"`         // Todoist API key
	Todoist_Project    string   `yaml:"todoist_project"` // Todoist Project ID
	Phisherman         string   `yaml:"phisherman"`      // Phisherman API key
	Google             string   `yaml:"google"`          // Google API key
	CSE                string   `yaml:"cse"`             // Custom Search Engine ID from Google
	MAL_ID             string   `yaml:"mal_id"`          // MyAnimeList API Client ID
	MAL_Secret         string   `yaml:"mal_secret"`      // MyAnimeList API Client Secret
	Invidious_URL      string   `yaml:"invidious"`       // Invidious URL
	OpenWeatherMap     string   `yaml:"weather"`         // OpenWeatherMap API key
	APIvoid            string   `yaml:"apivoid"`         // APIvoid API key
	Wolfram            string   `yaml:"wolfram"`         // Wolfram API key
	OpenAI             string   `yaml:"openai"`          // OpenAI API key
	AllowedRoles       []string `yaml:"allowedroles"`    // Allowed Discord roles
	EightballResponses []string `yaml:"8ball"`           // Responses used by 8ball
	Touhou             []string `yaml:"touhou"`          // Touhou search strings
	TrainingChannels   []string `yaml:"training"`        // Training channels for Brain
}

// Phisherman type
type Phisherman struct {
	Success        bool   `json:"success"`
	Message        string `json:"message"`
	Classification string `json:"classification"`
	Verified       bool   `json:"verifiedPhish"`
}

// Cringe Unified Manager AI type
type CUM struct {
	Manual bool // Manual state
	Value  int  // Value returned by cringe
}
