/*
	Bones :: General:

	General contains command functions which are
	deemed to not require a separate file. This is
	the "catch-all" file for small or singular command
	functions.

*/
package bones

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/hako/durafmt"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
)

// TwelveFeet - returns the input as a 12ft URL.
func TwelveFeet(input string) string {
	return "https://12ft.io/" + input
}

// CringeStateSet - sets cringe state.
func CringeStateSet(input string) string {
	Logging(Config, Session, CRINGE+INFO+"Setting cringe output value to: "+input)
	// Get the requested value as an int.
	value, err := strconv.Atoi(input)
	if err != nil {
		Logging(Config, Session, CRINGE+WARN+"User did not supply valid input!")
		return "Invalid input. Please supply a number."
	}
	Logging(Config, Session, CRINGE+INFO+"User supplied valid input! Setting.")
	// Set the cringe state.
	CringeState.Manual = true
	CringeState.Value = value
	return "Value set successfully. The next Cringe Unified Manager analysis will return " + input + "%"
}

// Cringe - calculates cringe using an AI.
func Cringe(m *discordgo.MessageCreate) {
	Logging(Config, Session, CRINGE+INFO+"Running a cringe analysis.")
	// It's all a show.
	Session.ChannelMessageSend(m.ChannelID, "Cringe analysis request accepted from "+m.Author.Username)
	skmessage, _ := Session.ChannelMessageSend(m.ChannelID, "Starting Skele's Choice Cringe Uniform Manager:tm: AI")
	r := rand.Intn(7) + 2
	for i := 0; i < r; i++ {
		skmessage, _ = Session.ChannelMessageEdit(skmessage.ChannelID, skmessage.ID, skmessage.Content+".")
		time.Sleep(time.Second)
	}
	message, _ := Session.ChannelMessageSend(m.ChannelID, "|- CRINGE UNIFORM MANAGER ONLINE.\n|- ANALYZING")
	r = rand.Intn(5) + 1
	for i := 0; i < r; i++ {
		message, _ = Session.ChannelMessageEdit(message.ChannelID, message.ID, message.Content+".")
		time.Sleep(time.Second)
	}
	message, _ = Session.ChannelMessageEdit(message.ChannelID, message.ID, "|- ANALYSIS COMPLETE.\n|- CALCULATING PERCENTAGE.")
	r = rand.Intn(4) + 2
	for i := 0; i < r; i++ {
		message, _ = Session.ChannelMessageEdit(message.ChannelID, message.ID, message.Content+".")
		time.Sleep(time.Second)
	}
	if CringeState.Manual {
		Session.ChannelMessageEdit(message.ChannelID, message.ID, "|- CRINGE PERCENTAGE: "+strconv.Itoa(CringeState.Value)+"%")
		CringeState.Manual = false
		CringeState.Value = 0
	} else {
		Session.ChannelMessageEdit(message.ChannelID, message.ID, "|- CRINGE PERCENTAGE: "+strconv.Itoa(rand.Intn(100))+"%")
	}
	Session.ChannelMessageEdit(skmessage.ChannelID, skmessage.ID, "\nThank you for choosing Skele's Choice!")
}

// aeburp - 1 in 1000 chance to explode.
func Aeburp() string {
	// Get a random number between 0 and 1000, if 42 then they explode!
	if 42 == rand.Intn(1000) {
		Logging(Config, Session, EXPLODE+INFO+"They exploded.")
		return "***You exploded.*** Shame."
	}
	Logging(Config, Session, EXPLODE+INFO+"They did not explode!")
	return "You did *not* explode. Lucky you."
}

// status - get system status.
// Returns: CPU utilization, memory utilization, host boot time + uptime
func Status() (discordgo.MessageEmbed, error) {
	Logging(Config, Session, STATUS+INFO+"Getting system stats.")
	// Get CPU info.
	cpuv, _ := cpu.Percent(time.Second, false)
	cpu_util := strconv.Itoa(int(cpuv[0]))
	// Get memory info.
	memv, _ := mem.VirtualMemory()
	mem_util := strconv.Itoa(int(memv.UsedPercent))
	// Get host uptime and boot time.
	host_info, _ := host.Info()
	uptime := time.Since(time.Unix(int64(host_info.BootTime), 0))
	uptime_pretty, _ := durafmt.ParseStringShort(uptime.String())
	boot_time := fmt.Sprintf("<t:%v>", host_info.BootTime)
	Logging(Config, Session, STATUS+INFO+"Generating status message.")
	// Generate the embedded message using the retrieved values.
	var message discordgo.MessageEmbed
	message.URL = "https://gitlab.com/drskelebones/skelebot"
	message.Title = "skelebot system status"
	message.Color = 6737151
	var footer discordgo.MessageEmbedFooter
	footer.Text = "Developed with <3 by Dr. Skelebones of Skele's Choice."
	message.Footer = &footer
	var cpu discordgo.MessageEmbedField
	var mem discordgo.MessageEmbedField
	var up discordgo.MessageEmbedField
	var boot discordgo.MessageEmbedField
	cpu.Name = "CPU Utilization"
	cpu.Value = cpu_util + "%"
	cpu.Inline = false
	mem.Name = "Memory Utilization"
	mem.Value = mem_util + "%"
	mem.Inline = true
	up.Name = "Uptime"
	up.Value = uptime_pretty.String()
	up.Inline = true
	boot.Name = "Boot Time"
	boot.Value = boot_time
	boot.Inline = true
	message.Fields = []*discordgo.MessageEmbedField{&boot, &up, &cpu, &mem}
	return message, nil
}

// pick - pick a number!
func Pick(input string) string {
	// If the user did not specify a range then default to 10.
	if input != "" {
		value, _ := strconv.Atoi(strings.TrimPrefix(input, (PREFIX + "pick ")))
		result := strconv.Itoa(rand.Intn(value))
		Logging(Config, Session, PICK+INFO+"Pick range of 0 to "+strings.TrimPrefix(input, (PREFIX+"pick "))+" result: "+result)
		return result
	} else {
		Logging(Config, Session, PICK+INFO+"No value range specified. Defaulting to 10.")
		return strconv.Itoa(rand.Intn(10))
	}
}

// choose - choose something
func Choose(input string) string {
	if input == "" {
		Logging(Config, Session, CHOOSE+INFO+"User did not provide choices.")
		return "Please provide options to choose from."
	}
	// Pick from the list provided by the user.
	Logging(Config, Session, CHOOSE+INFO+"Choosing something from "+strings.TrimPrefix(input, (PREFIX+"choose ")))
	options := strings.Split(strings.TrimPrefix(input, (PREFIX+"choose ")), ",")
	choice := options[rand.Intn(len(options))]
	Logging(Config, Session, CHOOSE+INFO+"Chose: "+choice)
	return choice
}

// eightBall - magic 8ball command.
func EightBall(question string) string {
	// Check to make sure the question isn't blank.
	if strings.Replace(question, " ", "", -1) == "" {
		Logging(Config, Session, EIGHTBALL+WARN+"Empty string! Cancelling!")
		return "You need to ask a question."
	}
	Logging(Config, Session, EIGHTBALL+INFO+"User question:"+question)
	// Select a random response.
	response := Config.EightballResponses[rand.Intn(len(Config.EightballResponses))]
	Logging(Config, Session, EIGHTBALL+INFO+"Responding with: "+response)
	return response
}

// dice - does dice roll.
func Dice(input string) (string, string) {
	// Split the input into the necessary parts.
	parts := strings.Split(input, "d")
	if len(parts) != 2 {
		Logging(Config, Session, DICE+WARN+"User did not provide valid input.")
		return "Invalid input. Please follow `[# of dice]d[face value of die]`.", ""
	}
	Logging(Config, Session, DICE+INFO+"Rolling dice using the following input:"+input)
	// Get the information about the roll in numerical values.
	die_count, _ := strconv.Atoi(strings.Replace(parts[0], " ", "", -1))
	if die_count > 20 || die_count < 1 {
		Logging(Config, Session, DICE+WARN+"User requested invalid die configuration!")
		return "Invalid input. Please specify an amount of dice *between 1 and 20*", ""
	}
	die_faces, _ := strconv.Atoi(parts[1])
	if die_faces < 4 {
		Logging(Config, Session, DICE+WARN+"User requested invalid die configuration!")
		return "Invalid input. Please specify die faces of *4 or more.*", ""
	}
	die_sum := 0
	die_results := make([]string, die_count)
	// Calculate the roll result.
	for i := 0; i < die_count; i++ {
		result := rand.Intn(die_faces-1) + 1
		die_results[i] = strconv.Itoa(result)
		die_sum += result
	}
	die_result := strings.Join(die_results, ", ")
	Logging(Config, Session, DICE+INFO+"Roll results: "+die_result)
	Logging(Config, Session, DICE+INFO+"Roll sum: "+strconv.Itoa(die_sum))
	return die_result, strconv.Itoa(die_sum)
}

// todoistRequest - make a todoist task creation request.
// VERY basic, need to expand.
// TODO: replace http with req or restly.
func TodoistRequest(todo string, user string) {
	// Get the task text.
	Logging(Config, Session, TODO+INFO+"Got a Todoist request! Request: "+todo)
	// Generate payload.
	payload := strings.NewReader(`{` + " " + ` "project_id": ` + Config.Todoist_Project + `,` + " " + ` "content": "` + "[" + user + "]" + todo + `",` + " " + ` "priority": 2` + ", " + ` "due_string": "today"` + " " + ` }`)
	// Create http client.
	client := &http.Client{}
	// Create request.
	req, err := http.NewRequest("POST", TODOIST_URL, payload)
	// Check error.
	if err != nil {
		Logging(Config, Session, TODO+ERR+"Error generating Todoist request!")
		return
	}
	// Add headers to request.
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+Config.Todoist)
	// Send request.
	res, err := client.Do(req)
	// Check error.
	if err != nil {
		Logging(Config, Session, TODO+ERR+"Error sending Todoist request!")
		return
	}
	// Capture response.
	defer res.Body.Close()
	// Read response.
	_, err = ioutil.ReadAll(res.Body)
	// Check error.
	if err != nil {
		Logging(Config, Session, TODO+ERR+"Error in handling Todoist response!")
		return
	}
	// Log.
	Logging(Config, Session, TODO+INFO+"Successfully made Todoist API request.")
}

// PhishermanCheck - check a URL against Phisherman!
// TODO: replace http with req.
func PhishermanCheck(check_url string) string {
	// Log.
	Logging(Config, Session, PHISH+INFO+"Phisherman check for: `"+check_url+"`")
	// Create http client.
	client := &http.Client{}
	// Log.
	Logging(Config, Session, PHISH+INFO+"Checking: `"+check_url+"`")
	// Generatr report body.
	check := strings.NewReader("")
	// Get domain.
	domain := strings.Replace(check_url, " ", "", -1)
	if strings.HasPrefix(strings.TrimSpace(check_url), "http") {
		Logging(Config, Session, PHISH+INFO+"Extracting domain from URL.")
		u, err := url.Parse(strings.Replace(check_url, " ", "", -1))
		if err != nil {
			Logging(Config, Session, PHISH+WARN+"Error parsing URL.")
			return "Error parsing URL! Did you submit a valid URL?"
		}
		domain = u.Host
	}
	Logging(Config, Session, PHISH+INFO+"Domain: "+domain)
	// Create request.
	req, err := http.NewRequest("PUT", PHISHERMAN_CHECK+domain, check)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH+ERR+"Error generating Phisherman report!")
		return "Error handling phish report!"
	}
	// Add headers to request.
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+Config.Phisherman)
	// Send request.
	res, err := client.Do(req)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH+ERR+"Error sending Phisherman report!")
		return "Error handling phish report!"
	}
	// Capture response.
	defer res.Body.Close()
	// Read response.
	body, err := ioutil.ReadAll(res.Body)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH+ERR+"Error in handling Phisherman check response!")
		return "Error handling phish check!"
	}
	// Unmarshal response.
	var phishermanResponse Phisherman
	json.Unmarshal([]byte(body), &phishermanResponse)
	// Log.
	if phishermanResponse.Verified == true {
		Logging(Config, Session, PHISH+INFO+"Phisherman classified the domain as "+phishermanResponse.Classification+" and is verified!")
		return ("Phisherman classifed the URL as " + phishermanResponse.Classification + " and is verified!")
	} else {
		Logging(Config, Session, PHISH+INFO+"Phisherman classified the domain as "+phishermanResponse.Classification+" and is not verified!")
		return ("Phisherman classifed the URL as " + phishermanResponse.Classification + " and is not verified!")
	}
	return "Something fucked up."
}

// PhishermanReport - report a URL to Phisherman!
// TODO: replace http with req.
func PhishermanReport(url string) string {
	// Log.
	Logging(Config, Session, PHISH+INFO+"Phisherman report for: `"+url+"`")
	// Create http client.
	client := &http.Client{}
	// Log.
	Logging(Config, Session, PHISH+INFO+"Reporting the URL: `"+url+"`")
	// Generatr report body.
	report := strings.NewReader(`{ "url":"` + strings.Replace(url, " ", "", -1) + `" }`)
	// Create request.
	req, err := http.NewRequest("PUT", PHISHERMAN_REPORT, report)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH+ERR+"Error generating Phisherman report!")
		return "Error handling phish report!"
	}
	// Add headers to request.
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+Config.Phisherman)
	// Send request.
	res, err := client.Do(req)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH+ERR+"Error sending Phisherman report!")
		return "Error handling phish report!"
	}
	// Capture response.
	defer res.Body.Close()
	// Read response.
	body, err := ioutil.ReadAll(res.Body)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH+ERR+"Error in handling Phisherman report response!")
		return "Error handling phish report!"
	}
	// Unmarshal response.
	var phishermanResponse Phisherman
	json.Unmarshal([]byte(body), &phishermanResponse)
	// Log.
	if phishermanResponse.Success {
		Logging(Config, Session, PHISH+INFO+"Phisherman report successful!")
		return "Successful!"
	} else {
		Logging(Config, Session, PHISH+INFO+"Phisherman report unsuccessful because: "+phishermanResponse.Message)
		return ("Report unsuccessful because: " + phishermanResponse.Message)
	}
	return "Something fucked up."
}
