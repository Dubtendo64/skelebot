package bones

import (
	"fmt"

	owm "github.com/briandowns/openweathermap"
	"github.com/bwmarrin/discordgo"
)

// GetWeather - gets the weather for a specifc location.
func GetWeather(location string) (discordgo.MessageEmbed, error) {
	Logging(Config, Session, WEATHER+INFO+"Getting weather for `"+location+"`.")
	var message discordgo.MessageEmbed
	// Generate OpenWeatherMap client.
	weather, err := owm.NewCurrent("C", "EN", Config.OpenWeatherMap)
	if err != nil {
		Logging(Config, Session, WEATHER+WARN+"Error making weather client.")
		return message, err
	}
	// Get weather information.
	err = weather.CurrentByName(location)
	if err != nil {
		Logging(Config, Session, WEATHER+WARN+"Error getting weather for `"+location+"`")
		return message, err
	}
	Logging(Config, Session, WEATHER+INFO+"Generating weather report.")
	// Generate embedded message.
	message.URL = "https://gitlab.com/drskelebones/skelebot"
	message.Title = "Weather by Skele's Choice"
	message.Color = 6737151
	var footer discordgo.MessageEmbedFooter
	footer.Text = "Developed with <3 by Dr. Skelebones of Skele's Choice."
	message.Footer = &footer
	var thumbnail discordgo.MessageEmbedThumbnail
	thumbnail.URL = fmt.Sprintf("http://openweathermap.org/img/wn/%s@2x.png", weather.Weather[0].Icon)
	message.Thumbnail = &thumbnail
	Logging(Config, Session, WEATHER+INFO+"Weather icon URL: `"+message.Thumbnail.URL+"`")
	city := weather.Name
	country := weather.Sys.Country
	message.Description = "Current weather for " + city + ", " + country + "."
	sunrise := discordgo.MessageEmbedField{"Sunrise", fmt.Sprintf("<t:%d>", weather.Sys.Sunrise), true}
	sunset := discordgo.MessageEmbedField{"Sunset", fmt.Sprintf("<t:%d>", weather.Sys.Sunset), true}
	description := discordgo.MessageEmbedField{"Description", weather.Weather[0].Description, false}
	temp := discordgo.MessageEmbedField{"Temperature", fmt.Sprintf("%.2f°C", weather.Main.Temp), true}
	temp_min := discordgo.MessageEmbedField{"Min Temperature", fmt.Sprintf("%.2f°C", weather.Main.TempMin), true}
	temp_max := discordgo.MessageEmbedField{"Max Temperature", fmt.Sprintf("%.2f°C", weather.Main.TempMax), true}
	feelslike := discordgo.MessageEmbedField{"Feels Like", fmt.Sprintf("%.2f°C", weather.Main.FeelsLike), true}
	humidity := discordgo.MessageEmbedField{"Humidity", fmt.Sprintf("%d%%", weather.Main.Humidity), true}
	wind := discordgo.MessageEmbedField{"Wind Speed", fmt.Sprintf("%.1f km/h", weather.Wind.Speed), true}
	message.Fields = []*discordgo.MessageEmbedField{&temp, &temp_min, &temp_max, &feelslike, &humidity, &wind, &description, &sunrise, &sunset}
	return message, nil
}
