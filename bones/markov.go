package bones

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/PullRequestInc/go-gpt3"
	"github.com/mb-14/gomarkov"
)

var Brain *gomarkov.Chain

func InitializeBrain() {
	Brain = gomarkov.NewChain(1)
	if c, err := BrainLoad("skele.brain"); err != nil {
		BrainTrain("training.txt", Brain)
	} else {
		Brain = c
	}
}

func ToggleLearning() {
	Config.Learning = !Config.Learning
}

func IsLearningChannel(check string) bool {
	for _, channel := range Config.TrainingChannels {
		if channel == check {
			return true
		}
	}
	return false
}

func IsMarkovCommand(check string) bool {
	return strings.HasPrefix(check, Config.Prefix+"markov")
}

func BrainLearn(input string, c *gomarkov.Chain) {
	Brain.Add(strings.Split(input, " "))
}

func BrainTrain(name string, c *gomarkov.Chain) error {
	file, err := os.Open(name)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		c.Add(strings.Split(scanner.Text(), " "))
	}
	return nil
}

func BrainLoad(name string) (*gomarkov.Chain, error) {
	var chain gomarkov.Chain
	data, err := ioutil.ReadFile(name)
	if err != nil {
		return &chain, err
	}
	err = json.Unmarshal(data, &chain)
	if err != nil {
		return &chain, err
	}
	return &chain, nil
}

func BrainSave(name string, c *gomarkov.Chain) {
	jsonObj, _ := json.Marshal(c)
	err := ioutil.WriteFile(name, jsonObj, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

func BrainGenerateResponse(chain *gomarkov.Chain) string {
	tokens := []string{gomarkov.StartToken}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := chain.Generate(tokens[(len(tokens) - 1):])
		tokens = append(tokens, next)
	}
	return strings.Join(tokens[1:len(tokens)-1], " ")
}

func BrainChat(chain *gomarkov.Chain, query string) string {
	var StartChat = `Human: Hello, who are you?
	AI: I am doing great. How can I help you today?
	`
	chatfile, err := os.OpenFile("chat.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Error opening chat.log. Creating.")
		chatfile, err = os.Create("chat.log")
		if err != nil {
			panic(err)
		}
	}
	defer chatfile.Close()
	chat, _ := ioutil.ReadFile("chat.log")
	if string(chat) == "" {
		chatfile.WriteString(StartChat)
	}

	ctx := context.Background()
	client := gpt3.NewClient(Config.OpenAI)
	var prompt []string = []string{string(chat) + "Human: " + query + "\nAI: "}
	var stops []string = []string{"\n", "AI: ", "Human: "}
	var temperature float32 = 0.9
	var presencepenalty float32 = 0.6
	var topp float32 = 1.0
	resp, err := client.Completion(ctx, gpt3.CompletionRequest{
		Prompt:          prompt,
		MaxTokens:       gpt3.IntPtr(30),
		Stop:            stops,
		TopP:            &topp,
		Temperature:     &temperature,
		PresencePenalty: presencepenalty,
	})
	if err != nil {
		log.Fatalln(err)
	}
	if resp.Choices[0].Text == "" {
		Logging(Config, Session, BRAIN+WARN+"OpenAI did not respond with anything. Defaulting to Markov.")
		return BrainGenerateResponse(chain)
	}

	return resp.Choices[0].Text
}
