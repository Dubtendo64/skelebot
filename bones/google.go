package bones

import (
	"errors"
	"math/rand"
	"net/http"
	"strings"

	"google.golang.org/api/customsearch/v1"
	"google.golang.org/api/googleapi/transport"
)

// googleSearch - returns the first link for a Google search.
func GoogleSearch(query string) (string, error) {
	// Create the HTTP client.
	client := &http.Client{Transport: &transport.APIKey{Key: Config.Google}}
	// Create the new service object using the HTTP client.
	svc, err := customsearch.New(client)
	if err != nil {
		Logging(Config, Session, GOOGLE+ERR+"Error creating Google search client!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!", err
	}
	// Perform the query.
	resp, err := svc.Cse.List().Cx(Config.CSE).Q(query).Do()
	if err != nil {
		Logging(Config, Session, GOOGLE+ERR+"Error handling Google search response!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!", err
	}
	// Check there are results then return the first link.
	if len(resp.Items) > 0 {
		Logging(Config, Session, GOOGLE+INFO+"Returning Google search result for: "+strings.TrimSpace(query))
		return resp.Items[0].Link, nil
	}
	Logging(Config, Session, GOOGLE+WARN+"Nothing found for '"+strings.TrimSpace(query)+"'!")
	// If no results then let the user know.
	return "Nothing found for '" + strings.TrimSpace(query) + "'!", errors.New("Error searching Google.")
}

// imageSearch - basic Google image search.
func ImageSearch(query string) (string, error) {
	// Create the HTTP client.
	client := &http.Client{Transport: &transport.APIKey{Key: Config.Google}}
	// Create the service object using the HTTP client.
	svc, err := customsearch.New(client)
	if err != nil {
		Logging(Config, Session, GOOGLE+ERR+"Error creating image search client!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!", errors.New("Nothing found with search.")
	}
	// Perform the image search query.
	resp, err := svc.Cse.List().Cx(Config.CSE).Q(query).SearchType("image").Do()
	if err != nil {
		Logging(Config, Session, GOOGLE+ERR+"Error handling image search response!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!", errors.New("Error handling search response.")
	}
	// If there are results then return the link of the first image.
	if len(resp.Items) > 0 {
		Logging(Config, Session, GOOGLE+INFO+"Returning image search result for: "+strings.TrimSpace(query))
		return resp.Items[0].Link, nil
	}
	Logging(Config, Session, GOOGLE+WARN+"Nothing found for '"+strings.TrimSpace(query)+"'!")
	// If no results then let the user know.
	return "Nothing found for '" + strings.TrimSpace(query) + "'!", errors.New("Nothing found with search.")
}

// ImageSearchRandom - basic Google image search but random return!.
func ImageSearchRandom(size int, query string) (string, error) {
	// Create the HTTP client.
	client := &http.Client{Transport: &transport.APIKey{Key: Config.Google}}
	// Create the service object using the HTTP client.
	svc, err := customsearch.New(client)
	if err != nil {
		Logging(Config, Session, GOOGLE+ERR+"Error creating image search client!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!", errors.New("Nothing found with search.")
	}
	// Perform the image search query.
	resp, err := svc.Cse.List().Cx(Config.CSE).Q(query).SearchType("image").Do()
	if err != nil {
		Logging(Config, Session, GOOGLE+ERR+"Error handling image search response!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!", errors.New("Error handling search response.")
	}
	// If there are results then return the link of the first image.
	if len(resp.Items) > 0 {
		Logging(Config, Session, GOOGLE+INFO+"Returning image search result for: "+strings.TrimSpace(query))
		if len(resp.Items) < size {
			return resp.Items[rand.Intn(len(resp.Items)-1)].Link, nil
		}
		return resp.Items[rand.Intn(size)].Link, nil
	}
	Logging(Config, Session, GOOGLE+WARN+"Nothing found for '"+strings.TrimSpace(query)+"'!")
	// If no results then let the user know.
	return "Nothing found for '" + strings.TrimSpace(query) + "'!", errors.New("Nothing found with search.")
}
