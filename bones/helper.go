package bones

import (
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// isModerator - checks to see if the member is a moderator.
func IsNotModerator(member *discordgo.Member) bool {
	for _, roleID := range member.Roles {
		for _, modRole := range Config.AllowedRoles {
			if roleID == modRole {
				return false
			}
		}
	}
	return true
}

// GetCommand - gets the command of a message.
func GetCommand(message string) string {
	parts := strings.Split(message, " ")
	return strings.TrimPrefix(parts[0], PREFIX)
}

// RemoveCommand - removes the command from a message.
func RemoveCommand(m string, c string) string {
	return strings.TrimSpace(strings.TrimPrefix(m, PREFIX+c))
}

// log - log something.
func Logging(config Configuration, session *discordgo.Session, input string) {
	log.Println(input)
	session.ChannelMessageSend(Config.Logging, input)
}
