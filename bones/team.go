package bones

import (
	"errors"
	"io/ioutil"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gopkg.in/yaml.v3"
)

type Member struct {
	Name           string   `yaml:"name"`
	Picture        string   `yaml:"picture"`
	Description    string   `yaml:"description"`
	AlternateNames []string `yaml:"alternate_names"`
	Roles          []string `yaml:"roles"`
	Projects       []string `yaml:"projects"`
	Country        string   `yaml:"country"`
	Location       string   `yaml:"location"`
	URL            string   `yaml:"url"`
	Git            string   `yaml:"git"`
	Twitter        string   `yaml:"twitter"`
	Discord        string   `yaml:"discord"`
	Email          string   `yaml:"email"`
	Roblox         string   `yaml:"roblox"`
	YouTube        string   `yaml:"youtube"`
	Steam          string   `yaml:"steam"`
	Itch           string   `yaml:"itch"`
	Kofi           string   `yaml:"kofi"`
	Linkedin       string   `yaml:"linkedin"`
}

type Team struct {
	Members []Member `yaml:"team"`
}

func GetTeamMember(name string, m *discordgo.MessageCreate) error {
	Logging(Config, Session, TEAM+INFO+"Getting team member bio for `"+name+"`.")
	file, err := ioutil.ReadFile("team.yml")
	if err != nil {
		Logging(Config, Session, TEAM+WARN+"Error opening team file.")
		return errors.New("Error opening team file.")
	}

	var team Team
	err = yaml.Unmarshal(file, &team)
	if err != nil {
		Logging(Config, Session, TEAM+WARN+"Error reading team file.")
		return errors.New("Error reading team file. " + err.Error())
	}
	member_index := FindTeamMember(team, name)
	if member_index >= 420 {
		Session.ChannelMessageSend(m.ChannelID, "Nothing found.")
		Logging(Config, Session, TEAM+WARN+"No team member found..")
		return errors.New("Error finding team member: " + name)
	}

	message := GenerateBio(team.Members[member_index])
	_, err = Session.ChannelMessageSendEmbed(m.ChannelID, &message)
	if err != nil {
		Logging(Config, Session, TEAM+WARN+"Error sending team member bio.")
		return errors.New("Error sending bio.")
	}
	return nil
}

func GenerateBio(member Member) discordgo.MessageEmbed {
	Logging(Config, Session, TEAM+INFO+"Generating bio for "+member.Name)
	var message discordgo.MessageEmbed
	if member.URL != "" {
		message.URL = member.URL
	}
	message.Title = member.Name
	message.Color = 6737151
	var footer discordgo.MessageEmbedFooter
	footer.Text = "Developed with <3 by Dr. Skelebones of Skele's Choice."
	message.Footer = &footer
	var thumbnail discordgo.MessageEmbedThumbnail
	thumbnail.URL = member.Picture
	message.Thumbnail = &thumbnail
	message.Description = member.Description
	var fields []*discordgo.MessageEmbedField
	if member.Discord != "" {
		discord := discordgo.MessageEmbedField{"Discord", member.Discord, true}
		fields = append(fields, &discord)
	}
	if len(member.Roles) > 0 {
		roles := discordgo.MessageEmbedField{"Roles", "", true}
		for _, role := range member.Roles {
			if roles.Value == "" {
				roles.Value = "" + role + ", "
			} else {
				roles.Value = roles.Value + role + ", "
			}
		}
		roles.Value = strings.TrimSuffix(roles.Value, ", ")
		fields = append(fields, &roles)
	}
	if member.Country != "" {
		country := discordgo.MessageEmbedField{"Country", member.Country, true}
		fields = append(fields, &country)
	}
	if member.Location != "" {
		location := discordgo.MessageEmbedField{"Location", member.Location, true}
		fields = append(fields, &location)
	}
	if member.Twitter != "" {
		twitter := discordgo.MessageEmbedField{"Twitter", member.Twitter, true}
		fields = append(fields, &twitter)
	}
	if member.Email != "" {
		email := discordgo.MessageEmbedField{"Email", member.Email, true}
		fields = append(fields, &email)
	}
	if member.Linkedin != "" {
		linkedin := discordgo.MessageEmbedField{"LinkedIn", member.Linkedin, true}
		fields = append(fields, &linkedin)
	}
	if member.Kofi != "" {
		kofi := discordgo.MessageEmbedField{"Ko-fi", member.Kofi, true}
		fields = append(fields, &kofi)
	}
	if member.Steam != "" {
		steam := discordgo.MessageEmbedField{"Steam", member.Steam, true}
		fields = append(fields, &steam)
	}
	if member.YouTube != "" {
		youtube := discordgo.MessageEmbedField{"YouTube", member.YouTube, true}
		fields = append(fields, &youtube)
	}
	if member.Roblox != "" {
		roblox := discordgo.MessageEmbedField{"Roblox", member.Roblox, true}
		fields = append(fields, &roblox)
	}
	if member.Git != "" {
		git := discordgo.MessageEmbedField{"Git", member.Git, true}
		fields = append(fields, &git)
	}
	if member.Itch != "" {
		itch := discordgo.MessageEmbedField{"Itch.io", member.Itch, true}
		fields = append(fields, &itch)
	}
	if len(member.Projects) > 0 {
		projects := discordgo.MessageEmbedField{"Projects", "", true}
		for _, project := range member.Projects {
			if projects.Value == "" {
				projects.Value = "" + project + ", "
			} else {
				projects.Value = projects.Value + project + ", "
			}
		}
		projects.Value = strings.TrimSuffix(projects.Value, ", ")
		fields = append(fields, &projects)
	}
	message.Fields = fields
	return message
}

func FindTeamMember(team Team, name string) int {
	Logging(Config, Session, TEAM+INFO+"Getting team member index.")
	for i := 0; i < len(team.Members); i++ {
		member := team.Members[i]
		if member.Name == name || strings.Contains(member.Name, name) || strings.Contains(strings.ToLower(member.Name), name) {
			return i
		}
		for j := 0; j < len(member.AlternateNames); j++ {
			alternate_name := member.AlternateNames[j]
			if alternate_name == name || strings.Contains(alternate_name, name) || strings.Contains(strings.ToLower(alternate_name), name) {
				return i
			}
		}
	}
	return 420
}
