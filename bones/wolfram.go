package bones

import (
	"strings"

	"github.com/imroc/req"
)

type Subpod struct {
	Title  string `json:"title"`
	Result string `json:"plaintext"`
}

type Pod struct {
	Title   string   `json:"title"`
	Subpods []Subpod `json:"subpods"`
}

type QueryResults struct {
	Successful bool  `json:"success"`
	Error      bool  `json:"error"`
	Pods       []Pod `json:"pods"`
}

type Search struct {
	Results QueryResults `json:"queryresult"`
}

const WOLFRAM = "https://api.wolframalpha.com/v2/query"

// CheckURL - check a URL agains URLvoid.
func Skelesearch(input string) (string, error) {
	Logging(Config, Session, SEARCH+INFO+"Searching for `"+input+"`")
	// Generate request client using these headers and params.
	header := req.Header{
		"Accept": "application/json",
	}
	param := req.Param{
		"input":  input,
		"format": "plaintext",
		"output": "JSON",
		"appid":  Config.Wolfram,
	}
	Logging(Config, Session, SEARCH+INFO+"Generating request client.")
	// Make request.
	r, err := req.Get(WOLFRAM, header, param)
	if err != nil {
		Logging(Config, Session, SEARCH+WARN+"Error generating request client.")
		return "Error generating request client.", err
	}
	var response Search
	Logging(Config, Session, SEC+INFO+"Parsing JSON response.")
	// Parse response from JSON.
	r.ToJSON(&response)

	for _, result := range response.Results.Pods {
		if result.Title == "Result" && result.Subpods[0].Result != "(data not available)" {
			return result.Subpods[0].Result, nil
		}
		if result.Title == "Exact result" {
			if strings.Contains(result.Subpods[0].Result, "(irreducible)") {
				return response.Results.Pods[2].Subpods[0].Result, nil
			}
			return result.Subpods[0].Result, nil
		}
	}

	result, err := GoogleSearch(input)
	if err != nil {
		return "Error searching for `" + input + "`.", err
	}

	return result, nil
}

// CheckURL - check a URL agains URLvoid.
func MathSolve(input string) (string, error) {
	Logging(Config, Session, SEARCH+INFO+"Solving math for `"+input+"`")
	// Generate request client using these headers and params.
	header := req.Header{
		"Accept": "application/json",
	}
	param := req.Param{
		"input":    input,
		"podstate": "Result__Step-by-step solution",
		"format":   "plaintext",
		"output":   "JSON",
		"appid":    Config.Wolfram,
	}
	Logging(Config, Session, SEARCH+INFO+"Generating request client.")
	// Make request.
	r, err := req.Get(WOLFRAM, header, param)
	if err != nil {
		Logging(Config, Session, SEARCH+WARN+"Error generating request client.")
		return "Error generating request client.", err
	}
	var response Search
	Logging(Config, Session, SEC+INFO+"Parsing JSON response.")
	// Parse response from JSON.
	r.ToJSON(&response)

	for _, result := range response.Results.Pods {
		if result.Title == "Results" || result.Title == "Exact result" {
			return "```perl\n" + result.Subpods[1].Result + "\n```", nil
		}
	}

	result, err := GoogleSearch(input)
	if err != nil {
		return "Error searching for `" + input + "`.", err
	}

	return result, nil
}
