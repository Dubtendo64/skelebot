package main

import "skelebot/bones"

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gopkg.in/yaml.v3"
)

// init - initializes things.
func init() {
	// Handle execution flags.
	flag.StringVar(&bones.Config.Config_File, "config", "skelebot.yml", "Config File")
	flag.Parse()
	// Some logging for which config file is being used.
	if bones.Config.Config_File == "skelebot.yml" {
		log.Println(bones.INFO + "Using adjacent config file: skelebot.yml")
	} else {
		log.Println(bones.INFO + "Using config file specified at: " + bones.Config.Config_File)
	}
	// Read config file.
	conf, err := ioutil.ReadFile(bones.Config.Config_File)
	if err != nil {
		log.Fatal(err)
	}
	// Unmarshal config file to the config object.
	err2 := yaml.Unmarshal(conf, &bones.Config)
	if err2 != nil {
		log.Fatal(err2)
	}
	if bones.Config.Prefix != "" || bones.Config.Prefix != " " {
		bones.PREFIX = bones.Config.Prefix
	}
	// Check to see if the Discord token was provided.
	// If none provided, then fatal log.
	if bones.Config.Token == "" {
		log.Fatal(bones.ERR + "Discord token must be provided!")
	}
	// Check to see if Todoist API token was provided.
	// If none provided, then warn log.
	if bones.Config.Todoist == "" {
		log.Println(bones.WARN + "No Todoist API key provided!")
	}
	// Check to see if Phisherman API token was provided.
	// If none provided, then warn log.
	if bones.Config.Phisherman == "" {
		log.Println(bones.WARN + "No Phsherman API key provided!")
	}
	bones.InitializeBrain()
}

// main
func main() {
	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + bones.Config.Token)
	if err != nil {
		log.Fatal(bones.ERR+"Error creating Discord session,", err)
		return
	}
	// Register the guildMessage function as a callback for MessageCreate events.
	dg.AddHandler(guildMessage)
	// Set intents on receiving guild message events.
	dg.Identify.Intents = discordgo.IntentsGuildMessages
	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		log.Fatal(bones.ERR+"Error opening connection,", err)
		return
	}
	bones.Session = dg
	// Print logo thing.
	fmt.Println(bones.SKELE)
	fmt.Println("\n=---------------- LOGGING START ----------------=")
	// Wait here until CTRL-C or other term signal is received.
	log.Println(bones.INFO + "Skelebot is now running. Press Ctrl-C to shutdown.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	bones.Logging(bones.Config, bones.Session, bones.INFO+"Shutting down Skelebot!")
	bones.BrainSave("skele.brain", bones.Brain)
	// Cleanly close down the Discord session.
	dg.Close()
}

// routeCommand - routes the command to the appropriate function.
func routeCommand(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Get prefix of message.
	command := bones.GetCommand(m.Content)
	// The big routing.
	switch command {
	case "ping", "pong":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a ping/pong request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, "Fuck you for pinging me, I'm here.")
		break
	case "todo", "task":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a Todoist request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		bones.TodoistRequest(strings.TrimPrefix(m.Content, (bones.PREFIX+command)), m.Author.Username)
		s.ChannelMessageSend(m.ChannelID, "Todoist request made by "+m.Author.Username+" for:"+strings.TrimPrefix(m.Content, (bones.PREFIX+command)))
		break
	case "yt", "youtube":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a YouTube search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, bones.YoutubeSearch(strings.TrimPrefix(m.Content, (bones.PREFIX+command))))
		break
	case "img", "image":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a Google image search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.ImageSearch(bones.RemoveCommand(m.Content, command))
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error processing Google image search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		}
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "g", "google":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a Google search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.GoogleSearch(bones.RemoveCommand(m.Content, command))
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error processing Google search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		}
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "pcheck":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a Phisherman check from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, "Checking provided URL via Phisherman!")
		response := bones.PhishermanCheck(strings.TrimPrefix(m.Content, (bones.PREFIX + command)))
		s.ChannelMessageSend(m.ChannelID, response)
		break
	case "preport":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a Phisherman report from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, "Reporting provided URL to Phisherman!")
		response := bones.PhishermanReport(strings.TrimPrefix(m.Content, (bones.PREFIX + command)))
		s.ChannelMessageSend(m.ChannelID, response)
		break
	case "8ball":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got 8ball request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, "Shaking eightball...")
		response := bones.EightBall(strings.TrimPrefix(m.Content, (bones.PREFIX + command)))
		s.ChannelMessageSend(m.ChannelID, ("The ball says: " + response))
		break
	case "flip", "coin":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a coin flip request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, "*DING!*")
		states := []string{"**Heads**", "**Tails**", "**Ricochet**"}
		s.ChannelMessageSend(m.ChannelID, states[rand.Intn(len(states))])
		break
	case "pick":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a number pick request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, bones.Pick(bones.RemoveCommand(m.Content, command)))
		break
	case "choose", "decide":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a choose request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, bones.Choose(bones.RemoveCommand(m.Content, command)))
		break
	case "aeburp", "explode", "aeurp":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got an explode request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, bones.Aeburp())
		break
	case "roll", "dice":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a roll request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		die_result, die_sum := bones.Dice(strings.TrimPrefix(m.Content, (bones.PREFIX + command)))
		if strings.HasPrefix(die_result, "Invalid") {
			s.ChannelMessageSend(m.ChannelID, die_result)
		} else {
			s.ChannelMessageSend(m.ChannelID, "**Dice**: "+die_result)
			s.ChannelMessageSend(m.ChannelID, "**Sum**: "+die_sum)
		}
		break
	case "status":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a status request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, _ := bones.Status()
		s.ChannelMessageSendEmbed(m.ChannelID, &out)
		break
	case "cringe":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a cringe analysis request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		bones.Cringe(m)
		break
	case "cringe_set":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a cringe state set request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, "Setting Cringe Unified Manager analysis state using supplied value.")
		out := bones.CringeStateSet(bones.RemoveCommand(m.Content, command))
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "mal", "anime":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a MyAnimeList search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, bones.MALSearch(bones.RemoveCommand(m.Content, command)))
		break
	case "src", "source":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a source request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, "https://gitlab.com/drskelebones/skelebot")
		break
	case "morse":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a morse encoding request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		morse := bones.Morse(bones.RemoveCommand(m.Content, command))
		s.ChannelMessageSend(m.ChannelID, morse)
		break
	case "demorse":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a morse decoding request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		demorse := bones.Demorse(bones.RemoveCommand(m.Content, command))
		s.ChannelMessageSend(m.ChannelID, demorse)
		break
	case "anilist":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got an Anilist search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out := bones.AnilistSearch(bones.RemoveCommand(m.Content, command))
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "invidious":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got an Invidious request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.GetInvidiousURL(bones.RemoveCommand(m.Content, command))
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error handling Invidious command.")
		}
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "w", "weather":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a weather request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.GetWeather(bones.RemoveCommand(m.Content, command))
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error handling weather command.")
			s.ChannelMessageSend(m.ChannelID, "Error getting weather for `"+bones.RemoveCommand(m.Content, command)+"`")
			break
		}
		s.ChannelMessageSendEmbed(m.ChannelID, &out)
		break
	case "urlcheck", "checkurl":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a URL check request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.CheckURL(bones.RemoveCommand(m.Content, command))
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error checking URL with APIvoid!")
			s.ChannelMessageSend(m.ChannelID, "Error checking URL.")
			break
		}
		s.ChannelMessageSendEmbed(m.ChannelID, &out)
		break
	case "2hu", "touhou":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a Touhou request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.GetTouhou()
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error getting Touhou!")
			s.ChannelMessageSend(m.ChannelID, "Error getting Touhou :((((")
			break
		}
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "skelesearch", "wolfram", "answer":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a search request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.Skelesearch(bones.RemoveCommand(m.Content, command))
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error getting search results!")
		}
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "skelesolve", "math":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a math solve request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out, err := bones.MathSolve(bones.RemoveCommand(m.Content, command))
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+"Error getting search results!")
		}
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "team", "developer":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a team bio request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		err := bones.GetTeamMember(bones.RemoveCommand(m.Content, command), m)
		if err != nil {
			bones.Logging(bones.Config, bones.Session, bones.COM+bones.WARN+err.Error())
		}
		break
	case "12ft", "paywall":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a 12ft.io request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		s.ChannelMessageSend(m.ChannelID, m.Author.Username+" sent > "+bones.TwelveFeet(bones.RemoveCommand(m.Content, command)))
		s.ChannelMessageDelete(m.ChannelID, m.ID)
		break
	case "markov":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a brain request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out := bones.BrainGenerateResponse(bones.Brain)
		s.ChannelMessageSend(m.ChannelID, out)
		break
	case "togglelearn":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got a brain learning toggle request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		bones.ToggleLearning()
		if bones.Config.Learning {
			s.ChannelMessageSend(m.ChannelID, "Brain toggled on.")
		} else {
			s.ChannelMessageSend(m.ChannelID, "Brain toggled off.")
		}
		break
	case "savebrain":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got brain save request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		bones.BrainSave("skele.brain", bones.Brain)
		s.ChannelMessageSend(m.ChannelID, "I saved my brain to: skele.brain")
		break
	case "chat":
		bones.Logging(bones.Config, bones.Session, bones.COM+"Got brain chat request from "+m.Author.Username+" in "+bones.Guild.Name+"!")
		out := bones.BrainChat(bones.Brain, bones.RemoveCommand(m.Content, command))
		s.ChannelMessageSend(m.ChannelID, out)
		break

	}
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func guildMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Get the member.
	member, err := s.GuildMember(m.GuildID, m.Author.ID)
	if err != nil {
		return
	}
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	// Learning!
	if bones.Config.Learning && bones.IsLearningChannel(m.ChannelID) && !bones.IsMarkovCommand(m.Content) {
		bones.BrainLearn(m.Content, bones.Brain)
		if 42 == rand.Intn(100) {
			out := bones.BrainGenerateResponse(bones.Brain)
			s.ChannelMessageSend(m.ChannelID, out)
		}
	}
	// Make sure user is moderator.
	if bones.IsNotModerator(member) {
		return
	}
	// Ignore all messages without prefix.
	if !strings.HasPrefix(m.Content, bones.PREFIX) {
		return
	}
	// Pass bones.Session to global
	bones.Session = s
	bones.Guild, _ = s.Guild(m.GuildID)
	// Route the command.
	// bones.TODO: candidate for goroutines, fucker.
	routeCommand(s, m)
}
