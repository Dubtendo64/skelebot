build: main.go bones/*
	@echo "[INFO] Building skelebot executable."
	go fmt main.go
	go fmt bones/*
	go build
clean: skelebot
	@echo "[WARN] Cleaning skelebot executable."
	rm skelebot
run: skelebot
	@echo "[INFO] Starting skelebot executable."
	./start-skelebot.sh
fresh: clean build run
	@echo "[INFO] Starting skelebot with fresh build."
