module skelebot

go 1.15

require (
	github.com/PullRequestInc/go-gpt3 v1.1.4
	github.com/alwindoss/morse v1.0.1
	github.com/briandowns/openweathermap v0.16.0
	github.com/bwmarrin/discordgo v0.23.2
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/hako/durafmt v0.0.0-20210608085754-5c1018a4e16b
	github.com/imroc/req v0.3.2
	github.com/kr/text v0.2.0 // indirect
	github.com/mb-14/gomarkov v0.0.0-20210216094942-a5b484cc0243
	github.com/nstratos/go-myanimelist v0.9.3
	github.com/raitonoberu/ytsearch v0.2.0
	github.com/shirou/gopsutil v3.21.11+incompatible
	github.com/texttheater/golang-levenshtein v1.0.1
	github.com/tidwall/gjson v1.12.1
	github.com/tklauser/go-sysconf v0.3.9 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
	golang.org/x/net v0.0.0-20211215060638-4ddde0e984e9 // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	golang.org/x/sys v0.0.0-20211214234402-4825e8c3871d // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/api v0.63.0
	google.golang.org/grpc v1.42.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
